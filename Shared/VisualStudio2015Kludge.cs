// This file is part of the TA.NetMF.TelecontrolliRainDetector project
// 
// Copyright � 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: VisualStudio2015Kludge.cs  Last modified: 2015-10-26@00:50 by Tim Long

namespace System.Diagnostics
    {
    public enum DebuggerBrowsableState
        {
        Never,
        Collapsed,
        RootHidden
        }
    }