// This file is part of the TA.NetMF.TelecontrolliRainDetector project
// 
// Copyright � 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: DbgSource.cs  Last modified: 2015-10-26@00:51 by Tim Long

using TA.NetMF.Utilities.Diagnostics;

namespace TA.NetMF.RainDetectorSampleApp
    {
    internal class DbgSource
        {
        public static readonly Source Application = new Source("SampleApp");
        }
    }