﻿// This file is part of the TA.NetMF.TelecontrolliRainDetector project
// 
// Copyright © 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: Program.cs  Last modified: 2015-10-26@00:50 by Tim Long

using System;
using System.Threading;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;
using TA.NetMF.Devices.Ssd1306;
using TA.NetMF.TelecontrolliRainDetector;
using TA.NetMF.Utilities.Diagnostics;
using TA.NetMF.Utilities.I2C;
using Timeout = TA.NetMF.Utilities.Timeout;
using Watchdog = TA.NetMF.Utilities.Watchdog;

namespace TA.NetMF.RainDetectorSampleApp
    {
    public class Program
        {
        static DisplayDriver display;
        static RainSensor rainSensor;
        static BitMappedFont numberFont;
        static BitMappedFont labelFont;
        static TrafficLight trafficLights;
        static TemperatureSensor temperatureSensor;
        static Timeout displayCycleTime;
        static DateTime displayModeLastChanged;
        static int displayMode;
        static readonly int numberOfDisplayModes = 4;
        static float percentWet;
        static double thermistorVolts;
        static double thermistorOhms;
        static double temperature;

        public static void Main()
            {
            var resetPin = new OutputPort(Pins.GPIO_PIN_D13, true);
            Watchdog.ConfigureResetOutput(resetPin);
            var watchdog = new Watchdog(Timeout.FromSeconds(4), "Main Loop");
            watchdog.Start();

            rainSensor = new RainSensor(Pins.GPIO_PIN_D0, 50);
            temperatureSensor = new TemperatureSensor(AnalogChannels.ANALOG_PIN_A0, 10);
            numberFont = new OcrA28PtMonospaced();
            labelFont = new SegoeUI10PtProportional();
            StartDisplay();

            trafficLights = new TrafficLight(Pins.GPIO_PIN_D7, Pins.GPIO_PIN_D6, Pins.GPIO_PIN_D5);
            trafficLights.AmberThreshold = 0.08;
            trafficLights.RedThreshold = 0.20;
            trafficLights.Value = 0;
            displayCycleTime = Timeout.FromSeconds(1.5);
            displayModeLastChanged = DateTime.UtcNow;
            temperatureSensor.Enable(4);
            rainSensor.Enable();
            while (true)
                {
                watchdog.Restart();
                ReadSensorValues();
                ScheduleDisplayActions();
                trafficLights.Value = percentWet;
                Thread.Sleep(250);
                }
            }

        static void ReadSensorValues()
            {
            percentWet = rainSensor.PercentWet;
            thermistorVolts = temperatureSensor.MovingAverageVoltage;
            thermistorOhms = temperatureSensor.MovingAverageResistance;
            temperature = temperatureSensor.MovingAverageTemperature;
            }

        static void ScheduleDisplayActions()
            {
            var elapsed = DateTime.UtcNow - displayModeLastChanged;
            if (elapsed >= displayCycleTime)
                {
                displayMode = ++displayMode % numberOfDisplayModes;
                SetDisplayMode(displayMode);
                displayModeLastChanged = DateTime.UtcNow;
                }
            switch (displayMode)
                {
                    case 0:
                        DisplayWetness();
                        break;
                    case 1:
                        DisplayVolts();
                        break;
                    case 2:
                        DisplayResistance();
                        break;
                    case 3:
                        DisplayTemperature();
                        break;
                    default:
                        displayMode = 0;
                        break;
                }
            }

        static void SetDisplayMode(int mode)
            {
            Dbg.Trace("Set display mode " + displayMode, DbgSource.Application);
            display.ClearScreen();
            }

        static void DisplayWetness()
            {
            var percentWetForDisplay = (int) (percentWet * 100.0);
            var displayString = percentWetForDisplay.ToString("D3") + "%";
            display.RenderString(numberFont, 1, 8, displayString);
            display.RenderString(labelFont, 6, 8, "Wetness");
            }

        static void DisplayTemperature()
            {
            var displayString = temperature.ToString("D1");
            display.RenderString(numberFont, 1, 8, displayString);
            display.RenderString(labelFont, 6, 8, "Temperature C");
            }

        static void DisplayVolts()
            {
            display.RenderString(numberFont, 1, 8, thermistorVolts.ToString("D2"));
            display.RenderString(labelFont, 6, 8, "Thermistor Volts");
            }

        static void DisplayResistance()
            {
            display.RenderString(numberFont, 1, 8, thermistorOhms.ToString("D0"));
            display.RenderString(labelFont, 6, 8, "Thermistor Ohms");
            }

        static void StartDisplay()
            {
            var configuration = new I2CDevice.Configuration(0x3C, 400);
            var iicDevice = new ThreadSafeI2CDevice(configuration, 3, 150);
            display = new DisplayDriver(iicDevice);
            display.PowerUp();
            display.ClearScreen();
            }
        }
    }