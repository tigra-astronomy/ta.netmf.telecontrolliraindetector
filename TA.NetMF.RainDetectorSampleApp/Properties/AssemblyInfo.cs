﻿// This file is part of the TA.NetMF.TelecontrolliRainDetector project
// 
// Copyright © 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: AssemblyInfo.cs  Last modified: 2015-10-26@00:50 by Tim Long

using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("TA.NetMF.RainDetectorSampleApp")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("TA.NetMF.RainDetectorSampleApp")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]