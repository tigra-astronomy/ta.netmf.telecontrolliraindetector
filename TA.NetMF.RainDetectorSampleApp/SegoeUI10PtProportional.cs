// This file is part of the TA.NetMF.TelecontrolliRainDetector project
// 
// Copyright � 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: SegoeUI10PtProportional.cs  Last modified: 2015-10-26@00:51 by Tim Long

using System.Collections;
using TA.NetMF.Devices.Ssd1306;

namespace TA.NetMF.RainDetectorSampleApp
    {
    public class SegoeUI10PtProportional : BitMappedFont
        {
        public SegoeUI10PtProportional()
            {
            CellPageHeight = 2;
            Characters = new Hashtable
                {
                {
                ' ', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //       
                        //       
                        //       
                        //       
                        //       
                        //       
                        //       
                        //       
                        //       
                        //       
                        //       
                        //       
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                '!', new BitMappedCharacter
                    {
                    Width = 1,
                    Stripes = new byte[]
                        {
                        //  
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        //  
                        // #
                        // #
                        //  
                        //  
                        //  
                        0x7E,
                        0x03
                        }
                    }
                },
                {
                '"', new BitMappedCharacter
                    {
                    Width = 3,
                    Stripes = new byte[]
                        {
                        //    
                        // # #
                        // # #
                        // # #
                        //    
                        //    
                        //    
                        //    
                        //    
                        //    
                        //    
                        //    
                        //    
                        0x0E, 0x00, 0x0E,
                        0x00, 0x00, 0x00
                        }
                    }
                },
                {
                '#', new BitMappedCharacter
                    {
                    Width = 8,
                    Stripes = new byte[]
                        {
                        //         
                        //    #  # 
                        //    #  # 
                        // ########
                        //   #  #  
                        //   #  #  
                        // ########
                        //  #  #   
                        //  #  #   
                        //         
                        //         
                        //         
                        //         
                        0x48, 0xC8, 0x78, 0x4E, 0xC8, 0x78, 0x4E, 0x48,
                        0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                '$', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //   #  
                        //  ####
                        // # # #
                        // # #  
                        // ###  
                        //  ### 
                        //   ###
                        //   # #
                        // # # #
                        // #### 
                        //   #  
                        //      
                        //      
                        0x1C, 0x32, 0xFF, 0x62, 0xC6,
                        0x03, 0x02, 0x07, 0x02, 0x01
                        }
                    }
                },
                {
                '%', new BitMappedCharacter
                    {
                    Width = 10,
                    Stripes = new byte[]
                        {
                        //           
                        //  ##     # 
                        // #  #   #  
                        // #  #  #   
                        // #  # #    
                        //  ## ## ## 
                        //     # #  #
                        //    #  #  #
                        //   #   #  #
                        //  #     ## 
                        //           
                        //           
                        //           
                        0x1C, 0x22, 0x22, 0x9C, 0x60, 0x30, 0xC8, 0x24, 0x22, 0xC0,
                        0x00, 0x02, 0x01, 0x00, 0x00, 0x00, 0x01, 0x02, 0x02, 0x01
                        }
                    }
                },
                {
                '&', new BitMappedCharacter
                    {
                    Width = 9,
                    Stripes = new byte[]
                        {
                        //          
                        //   ##     
                        //  #  #    
                        //  #  #  # 
                        //   ##   # 
                        //  # ##  # 
                        // #   ###  
                        // #    ##  
                        // #    ##  
                        //  ####  ##
                        //          
                        //          
                        //          
                        0xC0, 0x2C, 0x12, 0x32, 0x6C, 0xC0, 0xC0, 0x38, 0x00,
                        0x01, 0x02, 0x02, 0x02, 0x02, 0x01, 0x01, 0x02, 0x02
                        }
                    }
                },
                {
                '\'', new BitMappedCharacter
                    {
                    Width = 1,
                    Stripes = new byte[]
                        {
                        //  
                        // #
                        // #
                        // #
                        //  
                        //  
                        //  
                        //  
                        //  
                        //  
                        //  
                        //  
                        //  
                        0x0E,
                        0x00
                        }
                    }
                },
                {
                '(', new BitMappedCharacter
                    {
                    Width = 3,
                    Stripes = new byte[]
                        {
                        //    
                        //   #
                        //  # 
                        //  # 
                        // #  
                        // #  
                        // #  
                        // #  
                        // #  
                        // ## 
                        //  # 
                        //   #
                        //    
                        0xF0, 0x0C, 0x02,
                        0x03, 0x06, 0x08
                        }
                    }
                },
                {
                ')', new BitMappedCharacter
                    {
                    Width = 3,
                    Stripes = new byte[]
                        {
                        //    
                        // #  
                        //  # 
                        //  # 
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        //  ##
                        //  # 
                        // #  
                        //    
                        0x02, 0x0C, 0xF0,
                        0x08, 0x06, 0x03
                        }
                    }
                },
                {
                '*', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //   #  
                        // # # #
                        //  ### 
                        //  # # 
                        //      
                        //      
                        //      
                        //      
                        //      
                        //      
                        //      
                        //      
                        0x04, 0x18, 0x0E, 0x18, 0x04,
                        0x00, 0x00, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                '+', new BitMappedCharacter
                    {
                    Width = 7,
                    Stripes = new byte[]
                        {
                        //        
                        //        
                        //    #   
                        //    #   
                        //    #   
                        // #######
                        //    #   
                        //    #   
                        //    #   
                        //        
                        //        
                        //        
                        //        
                        0x20, 0x20, 0x20, 0xFC, 0x20, 0x20, 0x20,
                        0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                ',', new BitMappedCharacter
                    {
                    Width = 2,
                    Stripes = new byte[]
                        {
                        //   
                        //   
                        //   
                        //   
                        //   
                        //   
                        //   
                        //   
                        //   
                        //  #
                        // # 
                        // # 
                        //   
                        0x00, 0x00,
                        0x0C, 0x02
                        }
                    }
                },
                {
                '-', new BitMappedCharacter
                    {
                    Width = 3,
                    Stripes = new byte[]
                        {
                        //    
                        //    
                        //    
                        //    
                        //    
                        // ###
                        //    
                        //    
                        //    
                        //    
                        //    
                        //    
                        //    
                        0x20, 0x20, 0x20,
                        0x00, 0x00, 0x00
                        }
                    }
                },
                {
                '.', new BitMappedCharacter
                    {
                    Width = 1,
                    Stripes = new byte[]
                        {
                        //  
                        //  
                        //  
                        //  
                        //  
                        //  
                        //  
                        //  
                        // #
                        // #
                        //  
                        //  
                        //  
                        0x00,
                        0x03
                        }
                    }
                },
                {
                '/', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //     #
                        //    # 
                        //    # 
                        //    # 
                        //   #  
                        //   #  
                        //   #  
                        //  #   
                        //  #   
                        // #    
                        // #    
                        //      
                        0x00, 0x00, 0xE0, 0x1C, 0x02,
                        0x0C, 0x03, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                '0', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //  #### 
                        //  #  # 
                        // #    #
                        // #    #
                        // #    #
                        // #    #
                        // #    #
                        //  #  # 
                        //  #### 
                        //       
                        //       
                        //       
                        0xF8, 0x06, 0x02, 0x02, 0x06, 0xF8,
                        0x00, 0x03, 0x02, 0x02, 0x03, 0x00
                        }
                    }
                },
                {
                '1', new BitMappedCharacter
                    {
                    Width = 3,
                    Stripes = new byte[]
                        {
                        //    
                        //   #
                        // ###
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        //    
                        //    
                        //    
                        0x04, 0x04, 0xFE,
                        0x00, 0x00, 0x03
                        }
                    }
                },
                {
                '2', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //  ### 
                        // #   #
                        //     #
                        //     #
                        //    # 
                        //   #  
                        //  #   
                        // #    
                        // #####
                        //      
                        //      
                        //      
                        0x04, 0x82, 0x42, 0x22, 0x1C,
                        0x03, 0x02, 0x02, 0x02, 0x02
                        }
                    }
                },
                {
                '3', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //  ### 
                        // #   #
                        //     #
                        //     #
                        //  ### 
                        //     #
                        //     #
                        // #   #
                        // #### 
                        //      
                        //      
                        //      
                        0x04, 0x22, 0x22, 0x22, 0xDC,
                        0x03, 0x02, 0x02, 0x02, 0x01
                        }
                    }
                },
                {
                '4', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //     # 
                        //    ## 
                        //    ## 
                        //   # # 
                        //  #  # 
                        // ##  # 
                        // ######
                        //     # 
                        //     # 
                        //       
                        //       
                        //       
                        0xC0, 0xE0, 0x90, 0x8C, 0xFE, 0x80,
                        0x00, 0x00, 0x00, 0x00, 0x03, 0x00
                        }
                    }
                },
                {
                '5', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        // #####
                        // #    
                        // #    
                        // #### 
                        //    ##
                        //     #
                        //     #
                        // #  ##
                        // #### 
                        //      
                        //      
                        //      
                        0x1E, 0x12, 0x12, 0x32, 0xE2,
                        0x03, 0x02, 0x02, 0x03, 0x01
                        }
                    }
                },
                {
                '6', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //   ###
                        //  #   
                        // #    
                        // #### 
                        // ##  #
                        // #   #
                        // #   #
                        // ##  #
                        //  ### 
                        //      
                        //      
                        //      
                        0xF8, 0x34, 0x12, 0x12, 0xE2,
                        0x01, 0x03, 0x02, 0x02, 0x01
                        }
                    }
                },
                {
                '7', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        // #####
                        //     #
                        //    # 
                        //    # 
                        //   #  
                        //   #  
                        //   #  
                        //  #   
                        //  #   
                        //      
                        //      
                        //      
                        0x02, 0x02, 0xE2, 0x1A, 0x06,
                        0x00, 0x03, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                '8', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //  ### 
                        // #   #
                        // #   #
                        // #   #
                        //  ### 
                        // #   #
                        // #   #
                        // #   #
                        //  ### 
                        //      
                        //      
                        //      
                        0xDC, 0x22, 0x22, 0x22, 0xDC,
                        0x01, 0x02, 0x02, 0x02, 0x01
                        }
                    }
                },
                {
                '9', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //  ### 
                        // #  ##
                        // #   #
                        // #   #
                        // #   #
                        //  ####
                        //     #
                        //    # 
                        // ###  
                        //      
                        //      
                        //      
                        0x3C, 0x42, 0x42, 0x46, 0xFC,
                        0x02, 0x02, 0x02, 0x01, 0x00
                        }
                    }
                },
                {
                ':', new BitMappedCharacter
                    {
                    Width = 1,
                    Stripes = new byte[]
                        {
                        //  
                        //  
                        //  
                        // #
                        // #
                        //  
                        //  
                        //  
                        // #
                        // #
                        //  
                        //  
                        //  
                        0x18,
                        0x03
                        }
                    }
                },
                {
                ';', new BitMappedCharacter
                    {
                    Width = 2,
                    Stripes = new byte[]
                        {
                        //   
                        //   
                        //   
                        //  #
                        //  #
                        //   
                        //   
                        //   
                        //   
                        //  #
                        // # 
                        // # 
                        //   
                        0x00, 0x18,
                        0x0C, 0x02
                        }
                    }
                },
                {
                '<', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //      
                        //      
                        //     #
                        //   ## 
                        //  #   
                        // ##   
                        //   ## 
                        //     #
                        //      
                        //      
                        //      
                        //      
                        0x40, 0x60, 0x90, 0x90, 0x08,
                        0x00, 0x00, 0x00, 0x00, 0x01
                        }
                    }
                },
                {
                '=', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //       
                        //       
                        //       
                        // ######
                        //       
                        //       
                        // ######
                        //       
                        //       
                        //       
                        //       
                        //       
                        0x90, 0x90, 0x90, 0x90, 0x90, 0x90,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                '>', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //      
                        //      
                        // #    
                        //  ##  
                        //    # 
                        //    ##
                        //  ##  
                        // #    
                        //      
                        //      
                        //      
                        //      
                        0x08, 0x90, 0x90, 0x60, 0x40,
                        0x01, 0x00, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                '?', new BitMappedCharacter
                    {
                    Width = 4,
                    Stripes = new byte[]
                        {
                        //     
                        // ### 
                        //    #
                        //    #
                        //   # 
                        //  #  
                        //  #  
                        //     
                        //  #  
                        //  #  
                        //     
                        //     
                        //     
                        0x02, 0x62, 0x12, 0x0C,
                        0x00, 0x03, 0x00, 0x00
                        }
                    }
                },
                {
                '@', new BitMappedCharacter
                    {
                    Width = 10,
                    Stripes = new byte[]
                        {
                        //           
                        //    ####   
                        //  ##    ## 
                        //  # #### ##
                        // # ##  #  #
                        // # #   #  #
                        // # #   #  #
                        // # #  ## ##
                        //  # ## ### 
                        //  ##       
                        //    #####  
                        //           
                        //           
                        0xF0, 0x0C, 0xF4, 0x1A, 0x0A, 0x8A, 0xFA, 0x04, 0x8C, 0xF8,
                        0x00, 0x03, 0x02, 0x05, 0x05, 0x04, 0x05, 0x05, 0x01, 0x00
                        }
                    }
                },
                {
                'A', new BitMappedCharacter
                    {
                    Width = 8,
                    Stripes = new byte[]
                        {
                        //         
                        //    ##   
                        //    ##   
                        //   # ##  
                        //   #  #  
                        //   #  #  
                        //  #    # 
                        //  ###### 
                        //  #    # 
                        // #      #
                        //         
                        //         
                        //         
                        0x00, 0xC0, 0xB8, 0x86, 0x8E, 0xB8, 0xC0, 0x00,
                        0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02
                        }
                    }
                },
                {
                'B', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        // #### 
                        // #   #
                        // #   #
                        // #   #
                        // #### 
                        // #   #
                        // #   #
                        // #   #
                        // #### 
                        //      
                        //      
                        //      
                        0xFE, 0x22, 0x22, 0x22, 0xDC,
                        0x03, 0x02, 0x02, 0x02, 0x01
                        }
                    }
                },
                {
                'C', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //   ####
                        //  #   #
                        // #     
                        // #     
                        // #     
                        // #     
                        // #     
                        //  #   #
                        //   ### 
                        //       
                        //       
                        //       
                        0xF8, 0x04, 0x02, 0x02, 0x02, 0x06,
                        0x00, 0x01, 0x02, 0x02, 0x02, 0x01
                        }
                    }
                },
                {
                'D', new BitMappedCharacter
                    {
                    Width = 7,
                    Stripes = new byte[]
                        {
                        //        
                        // #####  
                        // #    # 
                        // #     #
                        // #     #
                        // #     #
                        // #     #
                        // #    ##
                        // #   ## 
                        // #####  
                        //        
                        //        
                        //        
                        0xFE, 0x02, 0x02, 0x02, 0x02, 0x84, 0xF8,
                        0x03, 0x02, 0x02, 0x02, 0x03, 0x01, 0x00
                        }
                    }
                },
                {
                'E', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        // #####
                        // #    
                        // #    
                        // #    
                        // #####
                        // #    
                        // #    
                        // #    
                        // #####
                        //      
                        //      
                        //      
                        0xFE, 0x22, 0x22, 0x22, 0x22,
                        0x03, 0x02, 0x02, 0x02, 0x02
                        }
                    }
                },
                {
                'F', new BitMappedCharacter
                    {
                    Width = 4,
                    Stripes = new byte[]
                        {
                        //     
                        // ####
                        // #   
                        // #   
                        // #   
                        // ####
                        // #   
                        // #   
                        // #   
                        // #   
                        //     
                        //     
                        //     
                        0xFE, 0x22, 0x22, 0x22,
                        0x03, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                'G', new BitMappedCharacter
                    {
                    Width = 7,
                    Stripes = new byte[]
                        {
                        //        
                        //   #####
                        //  ##   #
                        // ##     
                        // #      
                        // #   ###
                        // #     #
                        // #     #
                        //  #    #
                        //   #### 
                        //        
                        //        
                        //        
                        0xF8, 0x0C, 0x06, 0x02, 0x22, 0x22, 0xE6,
                        0x00, 0x01, 0x02, 0x02, 0x02, 0x02, 0x01
                        }
                    }
                },
                {
                'H', new BitMappedCharacter
                    {
                    Width = 7,
                    Stripes = new byte[]
                        {
                        //        
                        // #     #
                        // #     #
                        // #     #
                        // #     #
                        // #######
                        // #     #
                        // #     #
                        // #     #
                        // #     #
                        //        
                        //        
                        //        
                        0xFE, 0x20, 0x20, 0x20, 0x20, 0x20, 0xFE,
                        0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03
                        }
                    }
                },
                {
                'I', new BitMappedCharacter
                    {
                    Width = 1,
                    Stripes = new byte[]
                        {
                        //  
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        //  
                        //  
                        //  
                        0xFE,
                        0x03
                        }
                    }
                },
                {
                'J', new BitMappedCharacter
                    {
                    Width = 3,
                    Stripes = new byte[]
                        {
                        //    
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        // ## 
                        //    
                        //    
                        //    
                        0x00, 0x00, 0xFE,
                        0x02, 0x02, 0x01
                        }
                    }
                },
                {
                'K', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        // #    #
                        // #   # 
                        // #  #  
                        // # #   
                        // ##    
                        // # #   
                        // #  #  
                        // #   # 
                        // #    #
                        //       
                        //       
                        //       
                        0xFE, 0x20, 0x50, 0x88, 0x04, 0x02,
                        0x03, 0x00, 0x00, 0x00, 0x01, 0x02
                        }
                    }
                },
                {
                'L', new BitMappedCharacter
                    {
                    Width = 4,
                    Stripes = new byte[]
                        {
                        //     
                        // #   
                        // #   
                        // #   
                        // #   
                        // #   
                        // #   
                        // #   
                        // #   
                        // ####
                        //     
                        //     
                        //     
                        0xFE, 0x00, 0x00, 0x00,
                        0x03, 0x02, 0x02, 0x02
                        }
                    }
                },
                {
                'M', new BitMappedCharacter
                    {
                    Width = 10,
                    Stripes = new byte[]
                        {
                        //           
                        // ##      ##
                        // ##      ##
                        // ###    ###
                        // # #    # #
                        // # ##  ## #
                        // #  #  #  #
                        // #  ####  #
                        // #   ##   #
                        // #   ##   #
                        //           
                        //           
                        //           
                        0xFE, 0x0E, 0x38, 0xE0, 0x80, 0x80, 0xE0, 0x38, 0x0E, 0xFE,
                        0x03, 0x00, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x03
                        }
                    }
                },
                {
                'N', new BitMappedCharacter
                    {
                    Width = 8,
                    Stripes = new byte[]
                        {
                        //         
                        // ##     #
                        // ##     #
                        // # #    #
                        // #  #   #
                        // #  ##  #
                        // #   #  #
                        // #    # #
                        // #     ##
                        // #     ##
                        //         
                        //         
                        //         
                        0xFE, 0x06, 0x08, 0x30, 0x60, 0x80, 0x00, 0xFE,
                        0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03
                        }
                    }
                },
                {
                'O', new BitMappedCharacter
                    {
                    Width = 8,
                    Stripes = new byte[]
                        {
                        //         
                        //   ####  
                        //  #    # 
                        // #      #
                        // #      #
                        // #      #
                        // #      #
                        // #      #
                        //  #    # 
                        //   ####  
                        //         
                        //         
                        //         
                        0xF8, 0x04, 0x02, 0x02, 0x02, 0x02, 0x04, 0xF8,
                        0x00, 0x01, 0x02, 0x02, 0x02, 0x02, 0x01, 0x00
                        }
                    }
                },
                {
                'P', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        // #### 
                        // #   #
                        // #   #
                        // #   #
                        // #  ##
                        // #### 
                        // #    
                        // #    
                        // #    
                        //      
                        //      
                        //      
                        0xFE, 0x42, 0x42, 0x62, 0x3C,
                        0x03, 0x00, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                'Q', new BitMappedCharacter
                    {
                    Width = 8,
                    Stripes = new byte[]
                        {
                        //         
                        //   ####  
                        //  #    # 
                        // ##     #
                        // #      #
                        // #      #
                        // #      #
                        // ##     #
                        //  #    # 
                        //   ##### 
                        //       ##
                        //         
                        //         
                        0xF8, 0x8C, 0x02, 0x02, 0x02, 0x02, 0x04, 0xF8,
                        0x00, 0x01, 0x02, 0x02, 0x02, 0x02, 0x07, 0x04
                        }
                    }
                },
                {
                'R', new BitMappedCharacter
                    {
                    Width = 7,
                    Stripes = new byte[]
                        {
                        //        
                        // #####  
                        // #    # 
                        // #    # 
                        // #   ## 
                        // ####   
                        // #  ##  
                        // #   #  
                        // #    # 
                        // #    ##
                        //        
                        //        
                        //        
                        0xFE, 0x22, 0x22, 0x62, 0xD2, 0x1C, 0x00,
                        0x03, 0x00, 0x00, 0x00, 0x00, 0x03, 0x02
                        }
                    }
                },
                {
                'S', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //  ####
                        // #   #
                        // #    
                        // ##   
                        //   ## 
                        //    ##
                        //     #
                        // #   #
                        // #### 
                        //      
                        //      
                        //      
                        0x1C, 0x12, 0x22, 0x62, 0xC6,
                        0x03, 0x02, 0x02, 0x02, 0x01
                        }
                    }
                },
                {
                'T', new BitMappedCharacter
                    {
                    Width = 7,
                    Stripes = new byte[]
                        {
                        //        
                        // #######
                        //    #   
                        //    #   
                        //    #   
                        //    #   
                        //    #   
                        //    #   
                        //    #   
                        //    #   
                        //        
                        //        
                        //        
                        0x02, 0x02, 0x02, 0xFE, 0x02, 0x02, 0x02,
                        0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                'U', new BitMappedCharacter
                    {
                    Width = 7,
                    Stripes = new byte[]
                        {
                        //        
                        // #     #
                        // #     #
                        // #     #
                        // #     #
                        // #     #
                        // #     #
                        // #     #
                        // ##   ##
                        //  ##### 
                        //        
                        //        
                        //        
                        0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFE,
                        0x01, 0x03, 0x02, 0x02, 0x02, 0x03, 0x01
                        }
                    }
                },
                {
                'V', new BitMappedCharacter
                    {
                    Width = 8,
                    Stripes = new byte[]
                        {
                        //         
                        // #      #
                        //  #    # 
                        //  #    # 
                        //  #    # 
                        //   #  #  
                        //   #  #  
                        //   ####  
                        //    ##   
                        //    ##   
                        //         
                        //         
                        //         
                        0x02, 0x1C, 0xE0, 0x80, 0x80, 0xE0, 0x1C, 0x02,
                        0x00, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                'W', new BitMappedCharacter
                    {
                    Width = 12,
                    Stripes = new byte[]
                        {
                        //             
                        // #    ##    #
                        // ##   ##   ##
                        //  #   ##   # 
                        //  #   # #  # 
                        //  #  #  #  # 
                        //   # #  # #  
                        //   # #  # #  
                        //   ##    ##  
                        //    #    ##  
                        //             
                        //             
                        //             
                        0x06, 0x3C, 0xC0, 0x00, 0xE0, 0x1E, 0x0E, 0xF0, 0x00, 0xC0, 0x3C, 0x06,
                        0x00, 0x00, 0x01, 0x03, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00
                        }
                    }
                },
                {
                'X', new BitMappedCharacter
                    {
                    Width = 8,
                    Stripes = new byte[]
                        {
                        //         
                        // ##    ##
                        //  #    # 
                        //   #  #  
                        //   ###   
                        //    ##   
                        //   ###   
                        //   #  #  
                        //  #    # 
                        // ##    ##
                        //         
                        //         
                        //         
                        0x02, 0x06, 0xD8, 0x70, 0x70, 0x88, 0x06, 0x02,
                        0x02, 0x03, 0x00, 0x00, 0x00, 0x00, 0x03, 0x02
                        }
                    }
                },
                {
                'Y', new BitMappedCharacter
                    {
                    Width = 7,
                    Stripes = new byte[]
                        {
                        //        
                        // #     #
                        //  #   # 
                        //  #   # 
                        //   # #  
                        //   # #  
                        //    #   
                        //    #   
                        //    #   
                        //    #   
                        //        
                        //        
                        //        
                        0x02, 0x0C, 0x30, 0xC0, 0x30, 0x0C, 0x02,
                        0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                'Z', new BitMappedCharacter
                    {
                    Width = 7,
                    Stripes = new byte[]
                        {
                        //        
                        // #######
                        //      # 
                        //     #  
                        //     #  
                        //    #   
                        //   #    
                        //   #    
                        //  #     
                        // #######
                        //        
                        //        
                        //        
                        0x02, 0x02, 0xC2, 0x22, 0x1A, 0x06, 0x02,
                        0x02, 0x03, 0x02, 0x02, 0x02, 0x02, 0x02
                        }
                    }
                },
                {
                '[', new BitMappedCharacter
                    {
                    Width = 2,
                    Stripes = new byte[]
                        {
                        //   
                        // ##
                        // # 
                        // # 
                        // # 
                        // # 
                        // # 
                        // # 
                        // # 
                        // # 
                        // # 
                        // ##
                        //   
                        0xFE, 0x02,
                        0x0F, 0x08
                        }
                    }
                },
                {
                '\\', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        // #    
                        // #    
                        //  #   
                        //  #   
                        //   #  
                        //   #  
                        //   #  
                        //    # 
                        //    # 
                        //    # 
                        //     #
                        //      
                        0x06, 0x18, 0xE0, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x07, 0x08
                        }
                    }
                },
                {
                ']', new BitMappedCharacter
                    {
                    Width = 2,
                    Stripes = new byte[]
                        {
                        //   
                        // ##
                        //  #
                        //  #
                        //  #
                        //  #
                        //  #
                        //  #
                        //  #
                        //  #
                        //  #
                        // ##
                        //   
                        0x02, 0xFE,
                        0x08, 0x0F
                        }
                    }
                },
                {
                '^', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //   #   
                        //   ##  
                        //  #  # 
                        //  #  # 
                        // #    #
                        //       
                        //       
                        //       
                        //       
                        //       
                        //       
                        //       
                        0x20, 0x18, 0x06, 0x04, 0x18, 0x20,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                '_', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //      
                        //      
                        //      
                        //      
                        //      
                        //      
                        //      
                        //      
                        //      
                        //      
                        // #####
                        //      
                        0x00, 0x00, 0x00, 0x00, 0x00,
                        0x08, 0x08, 0x08, 0x08, 0x08
                        }
                    }
                },
                {
                '`', new BitMappedCharacter
                    {
                    Width = 2,
                    Stripes = new byte[]
                        {
                        // # 
                        //  #
                        //   
                        //   
                        //   
                        //   
                        //   
                        //   
                        //   
                        //   
                        //   
                        //   
                        //   
                        0x01, 0x02,
                        0x00, 0x00
                        }
                    }
                },
                {
                'a', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //      
                        //      
                        //  ### 
                        //     #
                        //     #
                        //  ####
                        // #   #
                        // #   #
                        //  ####
                        //      
                        //      
                        //      
                        0x80, 0x48, 0x48, 0x48, 0xF0,
                        0x01, 0x02, 0x02, 0x02, 0x03
                        }
                    }
                },
                {
                'b', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        // #     
                        // #     
                        // #     
                        // # ### 
                        // ##  ##
                        // #    #
                        // #    #
                        // #    #
                        // ##  ##
                        // # ### 
                        //       
                        //       
                        //       
                        0xFF, 0x10, 0x08, 0x08, 0x18, 0xF0,
                        0x03, 0x01, 0x02, 0x02, 0x03, 0x01
                        }
                    }
                },
                {
                'c', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //      
                        //      
                        //   ###
                        //  #   
                        // #    
                        // #    
                        // #    
                        // ##   
                        //  ####
                        //      
                        //      
                        //      
                        0xE0, 0x10, 0x08, 0x08, 0x08,
                        0x01, 0x03, 0x02, 0x02, 0x02
                        }
                    }
                },
                {
                'd', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //      #
                        //      #
                        //      #
                        //  ### #
                        // ##  ##
                        // #    #
                        // #    #
                        // #    #
                        // ##  ##
                        //  ### #
                        //       
                        //       
                        //       
                        0xF0, 0x18, 0x08, 0x08, 0x10, 0xFF,
                        0x01, 0x03, 0x02, 0x02, 0x01, 0x03
                        }
                    }
                },
                {
                'e', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //      
                        //      
                        //  ### 
                        // ##  #
                        // #   #
                        // #####
                        // #    
                        // ##   
                        //  ####
                        //      
                        //      
                        //      
                        0xF0, 0x58, 0x48, 0x48, 0x70,
                        0x01, 0x03, 0x02, 0x02, 0x02
                        }
                    }
                },
                {
                'f', new BitMappedCharacter
                    {
                    Width = 4,
                    Stripes = new byte[]
                        {
                        //   ##
                        //  #  
                        //  #  
                        // ####
                        //  #  
                        //  #  
                        //  #  
                        //  #  
                        //  #  
                        //  #  
                        //     
                        //     
                        //     
                        0x08, 0xFE, 0x09, 0x09,
                        0x00, 0x03, 0x00, 0x00
                        }
                    }
                },
                {
                'g', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //       
                        //       
                        //  ### #
                        // ##  ##
                        // #    #
                        // #    #
                        // #    #
                        // ##  ##
                        //  ### #
                        //      #
                        //     # 
                        // ##### 
                        0xF0, 0x18, 0x08, 0x08, 0x10, 0xF8,
                        0x11, 0x13, 0x12, 0x12, 0x19, 0x07
                        }
                    }
                },
                {
                'h', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        // #    
                        // #    
                        // #    
                        // # ## 
                        // ##  #
                        // #   #
                        // #   #
                        // #   #
                        // #   #
                        // #   #
                        //      
                        //      
                        //      
                        0xFF, 0x10, 0x08, 0x08, 0xF0,
                        0x03, 0x00, 0x00, 0x00, 0x03
                        }
                    }
                },
                {
                'i', new BitMappedCharacter
                    {
                    Width = 1,
                    Stripes = new byte[]
                        {
                        // #
                        //  
                        //  
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        //  
                        //  
                        //  
                        0xF9,
                        0x03
                        }
                    }
                },
                {
                'j', new BitMappedCharacter
                    {
                    Width = 3,
                    Stripes = new byte[]
                        {
                        //   #
                        //    
                        //    
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        //   #
                        // ## 
                        0x00, 0x00, 0xF9,
                        0x10, 0x10, 0x0F
                        }
                    }
                },
                {
                'k', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        // #    
                        // #    
                        // #    
                        // #  ##
                        // # ## 
                        // # #  
                        // ##   
                        // # #  
                        // # ## 
                        // #  ##
                        //      
                        //      
                        //      
                        0xFF, 0x40, 0xB0, 0x18, 0x08,
                        0x03, 0x00, 0x01, 0x03, 0x02
                        }
                    }
                },
                {
                'l', new BitMappedCharacter
                    {
                    Width = 1,
                    Stripes = new byte[]
                        {
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        //  
                        //  
                        //  
                        0xFF,
                        0x03
                        }
                    }
                },
                {
                'm', new BitMappedCharacter
                    {
                    Width = 9,
                    Stripes = new byte[]
                        {
                        //          
                        //          
                        //          
                        // # ##  ## 
                        // ##  ##  #
                        // #   #   #
                        // #   #   #
                        // #   #   #
                        // #   #   #
                        // #   #   #
                        //          
                        //          
                        //          
                        0xF8, 0x10, 0x08, 0x08, 0xF0, 0x10, 0x08, 0x08, 0xF0,
                        0x03, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x03
                        }
                    }
                },
                {
                'n', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //      
                        //      
                        // # ## 
                        // ##  #
                        // #   #
                        // #   #
                        // #   #
                        // #   #
                        // #   #
                        //      
                        //      
                        //      
                        0xF8, 0x10, 0x08, 0x08, 0xF0,
                        0x03, 0x00, 0x00, 0x00, 0x03
                        }
                    }
                },
                {
                'o', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //       
                        //       
                        //  #### 
                        // ##  ##
                        // #    #
                        // #    #
                        // #    #
                        // ##  ##
                        //  #### 
                        //       
                        //       
                        //       
                        0xF0, 0x18, 0x08, 0x08, 0x18, 0xF0,
                        0x01, 0x03, 0x02, 0x02, 0x03, 0x01
                        }
                    }
                },
                {
                'p', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //       
                        //       
                        // # ### 
                        // ##  ##
                        // #    #
                        // #    #
                        // #    #
                        // ##  ##
                        // # ### 
                        // #     
                        // #     
                        // #     
                        0xF8, 0x10, 0x08, 0x08, 0x18, 0xF0,
                        0x1F, 0x01, 0x02, 0x02, 0x03, 0x01
                        }
                    }
                },
                {
                'q', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //       
                        //       
                        //  ### #
                        // ##  ##
                        // #    #
                        // #    #
                        // #    #
                        // ##  ##
                        //  ### #
                        //      #
                        //      #
                        //      #
                        0xF0, 0x18, 0x08, 0x08, 0x10, 0xF8,
                        0x01, 0x03, 0x02, 0x02, 0x01, 0x1F
                        }
                    }
                },
                {
                'r', new BitMappedCharacter
                    {
                    Width = 4,
                    Stripes = new byte[]
                        {
                        //     
                        //     
                        //     
                        // # ##
                        // ##  
                        // #   
                        // #   
                        // #   
                        // #   
                        // #   
                        //     
                        //     
                        //     
                        0xF8, 0x10, 0x08, 0x08,
                        0x03, 0x00, 0x00, 0x00
                        }
                    }
                },
                {
                's', new BitMappedCharacter
                    {
                    Width = 4,
                    Stripes = new byte[]
                        {
                        //     
                        //     
                        //     
                        //  ###
                        // #   
                        // #   
                        //  ## 
                        //    #
                        //    #
                        // ### 
                        //     
                        //     
                        //     
                        0x30, 0x48, 0x48, 0x88,
                        0x02, 0x02, 0x02, 0x01
                        }
                    }
                },
                {
                't', new BitMappedCharacter
                    {
                    Width = 4,
                    Stripes = new byte[]
                        {
                        //     
                        //  #  
                        //  #  
                        // ####
                        //  #  
                        //  #  
                        //  #  
                        //  #  
                        //  #  
                        //  ###
                        //     
                        //     
                        //     
                        0x08, 0xFE, 0x08, 0x08,
                        0x00, 0x03, 0x02, 0x02
                        }
                    }
                },
                {
                'u', new BitMappedCharacter
                    {
                    Width = 5,
                    Stripes = new byte[]
                        {
                        //      
                        //      
                        //      
                        // #   #
                        // #   #
                        // #   #
                        // #   #
                        // #   #
                        // #  ##
                        //  ## #
                        //      
                        //      
                        //      
                        0xF8, 0x00, 0x00, 0x00, 0xF8,
                        0x01, 0x02, 0x02, 0x01, 0x03
                        }
                    }
                },
                {
                'v', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //       
                        //       
                        // #    #
                        //  #  # 
                        //  #  # 
                        //  #  # 
                        //   ##  
                        //   ##  
                        //   ##  
                        //       
                        //       
                        //       
                        0x08, 0x70, 0x80, 0x80, 0x70, 0x08,
                        0x00, 0x00, 0x03, 0x03, 0x00, 0x00
                        }
                    }
                },
                {
                'w', new BitMappedCharacter
                    {
                    Width = 9,
                    Stripes = new byte[]
                        {
                        //          
                        //          
                        //          
                        // #   #   #
                        // #   #   #
                        //  # # # # 
                        //  # # # # 
                        //  # # # # 
                        //  ##  ##  
                        //   #   #  
                        //          
                        //          
                        //          
                        0x18, 0xE0, 0x00, 0xE0, 0x18, 0xE0, 0x00, 0xE0, 0x18,
                        0x00, 0x01, 0x03, 0x00, 0x00, 0x01, 0x03, 0x00, 0x00
                        }
                    }
                },
                {
                'x', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //       
                        //       
                        // ##   #
                        //  #  # 
                        //   ### 
                        //   ##  
                        //   ##  
                        //  #  # 
                        // ##  ##
                        //       
                        //       
                        //       
                        0x08, 0x18, 0xE0, 0xE0, 0x30, 0x08,
                        0x02, 0x03, 0x00, 0x00, 0x03, 0x02
                        }
                    }
                },
                {
                'y', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //       
                        //       
                        // #    #
                        //  #  # 
                        //  #  # 
                        //  #  # 
                        //   ##  
                        //   ##  
                        //   ##  
                        //   #   
                        //   #   
                        // ##    
                        0x08, 0x70, 0x80, 0x80, 0x70, 0x08,
                        0x10, 0x10, 0x0F, 0x03, 0x00, 0x00
                        }
                    }
                },
                {
                'z', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //       
                        //       
                        // ######
                        //     # 
                        //    #  
                        //   ##  
                        //   #   
                        //  #    
                        // ######
                        //       
                        //       
                        //       
                        0x08, 0x08, 0xC8, 0x68, 0x18, 0x08,
                        0x02, 0x03, 0x02, 0x02, 0x02, 0x02
                        }
                    }
                },
                {
                '{', new BitMappedCharacter
                    {
                    Width = 3,
                    Stripes = new byte[]
                        {
                        //    
                        //  ##
                        //  # 
                        //  # 
                        //  # 
                        //  # 
                        // #  
                        //  # 
                        //  # 
                        //  # 
                        //  # 
                        //  ##
                        //    
                        0x40, 0xBE, 0x02,
                        0x00, 0x0F, 0x08
                        }
                    }
                },
                {
                '|', new BitMappedCharacter
                    {
                    Width = 1,
                    Stripes = new byte[]
                        {
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        // #
                        0xFF,
                        0x1F
                        }
                    }
                },
                {
                '}', new BitMappedCharacter
                    {
                    Width = 3,
                    Stripes = new byte[]
                        {
                        //    
                        // ## 
                        //  # 
                        //  # 
                        //  # 
                        //  # 
                        //   #
                        //  # 
                        //  # 
                        //  # 
                        //  # 
                        // ## 
                        //    
                        0x02, 0xBE, 0x40,
                        0x08, 0x0F, 0x00
                        }
                    }
                },
                {
                '~', new BitMappedCharacter
                    {
                    Width = 6,
                    Stripes = new byte[]
                        {
                        //       
                        //       
                        //       
                        //       
                        //       
                        // ###  #
                        // #  ###
                        //       
                        //       
                        //       
                        //       
                        //       
                        //       
                        0x60, 0x20, 0x20, 0x40, 0x40, 0x60,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                        }
                    }
                }
                };
            }
        }
    }