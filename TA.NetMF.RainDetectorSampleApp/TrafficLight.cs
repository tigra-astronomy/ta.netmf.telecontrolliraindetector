// This file is part of the TA.NetMF.TelecontrolliRainDetector project
// 
// Copyright � 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: TrafficLight.cs  Last modified: 2015-10-26@00:51 by Tim Long

using Microsoft.SPOT.Hardware;

namespace TA.NetMF.RainDetectorSampleApp
    {
    public class TrafficLight
        {
        readonly OutputPort greenPort;
        readonly OutputPort amberPort;
        readonly OutputPort redPort;
        double value;

        public TrafficLight(Cpu.Pin green, Cpu.Pin amber, Cpu.Pin red)
            {
            greenPort = new OutputPort(green, false);
            amberPort = new OutputPort(amber, false);
            redPort = new OutputPort(red, false);
            AmberThreshold = 1.0 / 3.0;
            RedThreshold = 2.0 / 3.0;
            Value = 0.0;
            }

        public double RedThreshold { get; set; }

        public double AmberThreshold { get; set; }

        public double Value
            {
            get { return value; }
            set
                {
                this.value = value;
                SetTrafficLights();
                }
            }

        void SetTrafficLights()
            {
            greenPort.Write(value < AmberThreshold);
            amberPort.Write(value >= AmberThreshold && value < RedThreshold);
            redPort.Write(value >= RedThreshold);
            }
        }
    }