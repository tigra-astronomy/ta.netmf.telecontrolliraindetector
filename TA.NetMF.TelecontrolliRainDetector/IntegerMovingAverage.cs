// This file is part of the TA.NetMF.TelecontrolliRainDetector project
// 
// Copyright � 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: IntegerMovingAverage.cs  Last modified: 2015-10-26@00:50 by Tim Long

using System;

namespace TA.NetMF.TelecontrolliRainDetector
    {
    public class IntegerMovingAverage
        {
        protected long[] samples;
        protected int index;
        readonly object sync = new object();
        long runningTotal;

        public long Average => runningTotal / samples.Length;

        public IntegerMovingAverage(int numSamples)
            {
            if (numSamples <= 0)
                {
                throw new ArgumentOutOfRangeException(
                    "numSamples can't be negative or 0.");
                }
            samples = new long[numSamples];
            index = 0;
            Clear();
            }

        public void AddSample(long value)
            {
            lock (sync)
                {
                runningTotal = runningTotal - samples[index] + value;
                samples[index++] = value;
                index %= samples.Length;
                }
            }

        public void Clear(long initialValue = 0)
            {
            lock (sync)
                {
                for (var i = 0; i < samples.Length; i++)
                    {
                    samples[i] = initialValue;
                    }
                runningTotal = initialValue * samples.Length;
                }
            }
        }
    }