// This file is part of the TA.NetMF.TelecontrolliRainDetector project
// 
// Copyright � 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: InterpolatedLookup.cs  Last modified: 2015-10-26@00:50 by Tim Long

using System.Collections;

namespace TA.NetMF.TelecontrolliRainDetector
    {
    internal class InterpolatedLookup
        {
        Hashtable table;
        readonly double lowestKey;
        readonly double highestKey;
        readonly double[] keys;
        readonly double[] values;
        readonly int count;
        readonly int lastIndex;

        public InterpolatedLookup(double[] keys, double[] values)
            {
            this.keys = keys;
            this.values = values;
            count = keys.Length;
            lastIndex = count - 1;
            lowestKey = keys[0];
            highestKey = keys[lastIndex];
            }

        public InterpolatedLookup(Hashtable table)
            {
            count = table.Count;
            lastIndex = count - 1;
            keys = new double[count];
            values = new double[count];
            table.Keys.CopyTo(keys, 0);
            table.Values.CopyTo(values, 0);
            lowestKey = keys[0];
            highestKey = keys[lastIndex];
            }

        public double Lookup(double lookup)
            {
            // Shortcut: If table contains only 1 element, return that value.
            if (count == 1)
                return values[0];
            // Shortcut: If the lookup value is off the end of the table, return an end value.
            if (lookup <= lowestKey) return values[0];
            if (lookup >= highestKey) return values[lastIndex];
            /*
             * At this point, we know that the lookup value is >= first key and <= last key.
             * Find the key pair that surrounds the lookup value, such that k1 <= lookup < k2
             */
            var index = 0;
            while (keys[index] < lookup)
                {
                ++index;
                }
            var upperKey = keys[index];
            var upperValue = values[index];
            --index;
            var lowerKey = keys[index];
            var lowerValue = values[index];
            var keySpan = upperKey - lowerKey;
            var interpolatedDistance = (lookup - lowerKey) / keySpan; // fraction of unity
            var valueSpan = upperValue - lowerValue;
            var interpolatedValue = lowerValue + valueSpan * interpolatedDistance;
            return interpolatedValue;
            }
        }
    }