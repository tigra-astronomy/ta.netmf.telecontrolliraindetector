// This file is part of the TA.NetMF.TelecontrolliRainDetector project
// 
// Copyright � 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: RainSensor.cs  Last modified: 2015-10-26@00:50 by Tim Long

using System;
using System.Threading;
using Microsoft.SPOT.Hardware;

namespace TA.NetMF.TelecontrolliRainDetector
    {
    public class RainSensor
        {
        readonly IntegerMovingAverage samples;
        Timer sampleTimer;
        readonly Cpu.Pin pin;
        InterruptPort port;
        DateTime lastTime;

        public static long MaximumReading { get; private set; } = 30000;

        public static long MinimumReading { get; private set; } = 9000;

        public static long Range => MaximumReading - MinimumReading;

        public float Scale { get; } = 1f / Range;

        public RainSensor(Cpu.Pin digitalPin, int movingAverageSamples = 10)
            {
            pin = digitalPin;
            samples = new IntegerMovingAverage(movingAverageSamples);
            }

        public void Enable()
            {
            if (port != null) Disable();
            samples.Clear(MinimumReading);
            lastTime = DateTime.Now;
            port = new InterruptPort(pin, true, Port.ResistorMode.Disabled, Port.InterruptMode.InterruptEdgeHigh);
            port.OnInterrupt += TakeSample;
            port.EnableInterrupt();
            }

        public void Disable()
            {
            if (port != null)
                {
                port.DisableInterrupt();
                port.OnInterrupt -= TakeSample;
                port.Dispose();
                port = null;
                }
            }

        void TakeSample(uint data1, uint data2, DateTime time)
            {
            if (port == null)
                return; // Interrupts can still happen even after the handler has been unregistered!
            var elapsed = time - lastTime;
            lastTime = time;
            var ticks = elapsed.Ticks;
            samples.AddSample(ticks);
            InstantaneousValue = ticks;
            port.ClearInterrupt();
            }


        public long InstantaneousValue { get; set; }

        public long MovingAverageValue => samples.Average;

        /// <summary>
        ///     Gets the percentage wetness as a fraction of unity.
        /// </summary>
        /// <value>The percentage of wetness of the sensor, as a value from 0.0 to 1.0 inclusive.</value>
        public float PercentWet
            {
            get
                {
                var percentage = (MovingAverageValue - MinimumReading) * Scale;
                if (percentage > 1f) return 1f;
                if (percentage < 0f) return 0f;
                return percentage;
                }
            }
        }
    }