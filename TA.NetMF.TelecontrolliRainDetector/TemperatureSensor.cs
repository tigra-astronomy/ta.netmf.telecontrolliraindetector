// This file is part of the TA.NetMF.TelecontrolliRainDetector project
// 
// Copyright � 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: TemperatureSensor.cs  Last modified: 2015-10-26@00:50 by Tim Long

using System;
using System.Threading;
using Microsoft.SPOT.Hardware;
using Timeout = TA.NetMF.Utilities.Timeout;

namespace TA.NetMF.TelecontrolliRainDetector
    {
    public class TemperatureSensor
        {
        readonly double vRef;
        readonly double rSeries;
        readonly double rParallel;
        static readonly double[] resistances =
            {
            144.8, 166.2, 191.4, 221.2, 256.7, 299.2, 350.2, 411.8, 486.6, 577.8, 689.9, 828.2, 1000.0,
            1215.0, 1486.0, 1829.0, 2267.0, 2832.0, 3564.0
            };
        static readonly double[] temperatures =
            {
            85.0, 80.0, 75.0, 70.0, 65.0, 60.0, 55.0, 50.0, 45.0, 40.0,
            35.0, 30.0, 25.0, 20.0, 15.0, 10.0, 5.0, 0.0, -5.0
            };
        readonly InterpolatedLookup interpolator;
        readonly DoubleMovingAverage samples;
        readonly AnalogInput sensor;
        Thread sampleThread;
        Timeout sleepTime;
        bool enabled;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TemperatureSensor" /> class.
        /// </summary>
        /// <param name="analogChannel">The Analogue-toDigital converter channel to be used for measuring.</param>
        /// <param name="movingAverageWindow">The number of samples in the moving average window.</param>
        /// <param name="vRef">The analogue reference voltage, typically Vcc. Optional; defaults to 3.3V.</param>
        /// <param name="rSeries">The value of the series resistor. Optional; defaults to 5K6 Ohms.</param>
        /// <param name="rParallel">The value of the parallel resistor. Optional; defaults to 10K Ohms.</param>
        /// <remarks>
        ///     Voltage is measured directly by the A to D converter and is scaled to a value between 0V and VRef. The
        ///     thermistor resistance is computed from the measured voltage and the values of VRef, plus the series and
        ///     parallel resistors. Temperature (in Celsius) is obtained by from a lookup table of resistance vs.
        ///     temperature, using linear interpolation between the values in the table. The interpolation introduces a
        ///     small error but it is less than the tolerance in the components. The result is a temperature that is
        ///     accurate to about �2 degrees over a temperature range of -20 to +40. This is perfectly adequate for
        ///     the intended purpose of the sensor, which is to measure the temperature of the rain sensor substrate for
        ///     purposes of controlling the heater. It could also be used as an approximate measurement of ambient temperature,
        ///     but results will be skewed while the heater is active and that must be allowed for.
        /// </remarks>
        public TemperatureSensor(Cpu.AnalogChannel analogChannel, int movingAverageWindow = 10, double vRef = 3.3, double rSeries = 5600,
            double rParallel = 10000)
            {
            this.vRef = vRef;
            this.rSeries = rSeries;
            this.rParallel = rParallel;
            sensor = new AnalogInput(analogChannel);
            sensor.Scale = this.vRef;
            samples = new DoubleMovingAverage(movingAverageWindow);
            interpolator = new InterpolatedLookup(resistances, temperatures);
            }

        /// <summary>
        ///     Enables sampling of the temperature sensor at the specified sampling rate.
        /// </summary>
        /// <param name="samplesPerSecond">
        ///     The number of samples per second. Optional; defaults to 1 sample per second. There is probably no advantage in
        ///     going any faster
        ///     than that.
        /// </param>
        /// <exception cref="System.ArgumentOutOfRangeException">Maximum 1,000; strictly positive</exception>
        /// <exception cref="ArgumentOutOfRangeException">samplesPerSecond</exception>
        public void Enable(int samplesPerSecond = 1)
            {
            if (samplesPerSecond <= 0 || samplesPerSecond > 1000)
                throw new ArgumentOutOfRangeException(nameof(samplesPerSecond), "Maximum 1,000; strictly positive");
            Disable();
            samples.Clear(InstantaneousVoltage); // quickly stabilizes the moving average
            sleepTime = Timeout.FromSeconds(1.0 / samplesPerSecond);
            enabled = true;
            sampleThread = new Thread(SensorLoop);
            sampleThread.Priority = ThreadPriority.BelowNormal;
            sampleThread.Start();
            }

        /// <summary>
        ///     Gets the instantaneous sensor voltage in the range 0 to <c>vRef</c> (specified in the constructor).
        /// </summary>
        /// <value>The instantaneous value.</value>
        public double InstantaneousVoltage => sensor.Read();

        /// <summary>
        ///     Disables automatic sensor sampling. The moving average value is preserved and can still be read
        ///     when sampling is disabled, although obviously it will no longer update.
        /// </summary>
        public void Disable()
            {
            enabled = false;
            if (sampleThread != null)
                {
                sampleThread.Join(sleepTime.TimeSpan);
                sampleThread = null;
                }
            }

        void SensorLoop()
            {
            while (enabled)
                {
                samples.AddSample(InstantaneousVoltage);
                Thread.Sleep(sleepTime); // Allows another thread to be scheduled
                }
            }

        /// <summary>
        ///     Gets the moving average sensor voltage.
        /// </summary>
        /// <value>The moving average voltage value, computed over the number of samples specified in the constructor.</value>
        public double MovingAverageVoltage => samples.Average;

        /// <summary>
        ///     Computes the resistance of the sensor from the voltage measured on the analogue-to-digital converter.
        /// </summary>
        /// <param name="measuredVoltage">The voltage measured at the analogue to digital converter.</param>
        /// <returns>The computed resistance of the temperature sensor, in Ohms.</returns>
        double ComputeResistance(double measuredVoltage)
            {
            var R = -(measuredVoltage * rSeries * rParallel) / (measuredVoltage * (rSeries + rParallel) - (vRef * rParallel));
            return R;
            }

        /// <summary>
        ///     Gets the temperature in degrees Celsius based on the moving average resistance measurement.
        ///     Uses a lookup table and interpolation to determine the temperature from measured resistance.
        ///     Accuracy is about +/- 2 degrees, mostly due to the tolerance of the sensor.
        /// </summary>
        /// <value>The interpolated temperature, in degrees Celsius.</value>
        public double MovingAverageTemperature => interpolator.Lookup(MovingAverageResistance);

        public double MovingAverageResistance => ComputeResistance(MovingAverageVoltage);
        }
    }